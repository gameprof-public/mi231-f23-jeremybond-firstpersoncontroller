using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonController : MonoBehaviour {
    [Header( "Inscribed" )]
    public float speed = 10;
    public float     jumpVel     = 10;
    public float     yawMult     = 90;
    public float     pitchMult   = 45;
    public Vector2   pitchMinMax = new Vector2( -60, 60 );
    public bool      invertPitch = true;
    public Transform camTrans;
    
    
    
    
    private Rigidbody rigid;
    
    // Start is called before the first frame update
    void Start() {
        rigid = GetComponent<Rigidbody>();
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update() {
        // Pull all Input axis info
        float h = Input.GetAxis( "Horizontal" );
        float v = Input.GetAxis( "Vertical" );
        float yaw = Input.GetAxis( "Mouse X" );
        float pitch = Input.GetAxis( "Mouse Y" );
        
        
        // Handle Rotation: Yaw the Player
        Vector3 rot = transform.localEulerAngles;
        rot.y += yaw * yawMult * Time.deltaTime;
        transform.localEulerAngles = rot;
        
        
        // Handle Rotation: Pitch the Camera
        rot = camTrans.localEulerAngles;
        if ( rot.x > 180 ) rot.x -= 360;
        rot.x += pitch * pitchMult * Time.deltaTime * (invertPitch ? -1 : 1);
        rot.x = Mathf.Clamp( rot.x, pitchMinMax[0], pitchMinMax[1] );
        camTrans.localEulerAngles = rot;
        
        
        // Handle Movement
        Vector3 vel = Vector3.zero;
        vel += transform.right * h;
        vel += transform.forward * v;
        if (vel.magnitude > 1) vel.Normalize();
        vel *= speed;
        
        // Pull in the existing y velocity
        vel.y = rigid.velocity.y;
        // Handle a Jump
        if ( Input.GetKeyDown( KeyCode.Space ) ) {
            vel.y = jumpVel;
        }

        rigid.velocity = vel;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadBob : MonoBehaviour {
    [Header( "Inscribed" )]
    [Range(0,1)]
    public float bobAmount = 0.1f;
    public float bobFrequency = 0.1f;

    [Header( "Dynamic" )]
    public Vector3 bobDelta;
    public float bobProgression; 

    private Rigidbody rigid;
    private Vector3   basePos;
    
    void Start() {
        rigid = GetComponentInParent<Rigidbody>();
        bobDelta = Vector3.zero;
        basePos = transform.localPosition;
    }

    // Update is called once per frame
    void Update() {
        Vector3 vel = rigid.velocity;
        if ( Mathf.Abs( vel.y ) < 0.01f ) {
            bobProgression += bobFrequency * Mathf.PI * 2 * Time.deltaTime * vel.magnitude;
        }
        bobDelta.y = Mathf.Sin( bobProgression ) * bobAmount;
        transform.localPosition = basePos + bobDelta;
    }
}
